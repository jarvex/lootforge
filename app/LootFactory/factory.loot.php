<?php
class LootFactory
{
	// For testing purposes, refactor this class since it's messy
	
	
	// Tips for selecting an item:
	// 1.) CATEGORY
	// 2.) SUBCATEGORY
	// 3.) 
	public static function getCategory() {
		$categories = array(
			LOOT_CATEGORY_WEAPON => 50,
			LOOT_CATEGORY_CONTAINER => 0
		);
		return randomElementWeighted($categories);
	}

    public static function getTemplate($category = LOOT_CATEGORY_RANDOM) {
		if ($category == LOOT_CATEGORY_RANDOM) {
			$category = self::getCategory();
		}
		switch($category) {
			case LOOT_CATEGORY_WEAPON:
				return new Weapon($category);
			break;
			case LOOT_CATEGORY_CONTAINER:
				return new Container($category);
			break;
		}
    }
    
	public static function getContainer($depth = 1) {
		return new Container($depth);
	}
}
?>