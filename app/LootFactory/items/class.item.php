<?php
abstract class Item
{
	public $db;
	public $dtables;
	public $item = array();

	// Constructor
	public function __construct( int $category ) {
		$this->db 		= Registry::getInstance()->get('database');

		// Extract a random item template from our static database data.
		$item = $this->fetchTemplate($category);
		$this->setLoot($item);
		
		// Is this item possibly interesting / different from the normal?
		$this->randomizeQuality(TRUE); /* void & bool */
		// Parent class checks
		$this->checkQuality();

		// How many were found? (to-do: possibly loop through all stacks with different effects.)
		$this->randomizeQuantity();

		// Forge our new item with a material (to-do: add filter system)
		$mat = $this->getMaterial();
		$mat = $this->fetchMaterial($mat);
		$this->setMaterial($mat);

		$this->checkWeight();
		$this->checkPrice();
		$this->checkName();
		$this->checkMisc();
	}

	// General Knowledge
	// Intelligent items: Less than 1% are intelligent. Cannot be an item with charges or single use.
	// Cursed items: If randomly generated, 5% chance.
	
	// Getters
	public function getLoot() { return $this->item; }
	public function getName() { return $this->item['name']; }

	// Item Categories:
	// 0 - Wondrous Items / Miscellaneous
	// 1 - Weapons & Armor (Armaments)
	// 2 - Potions
	// 3 - Rings
	// 4 - Rods, Staves & Wands
	// 5 - Scrolls
	public function getCategory() { return $this->item['category']; }

	// Item Qualities:
	// 0 - Substandard (broken, fragile, destroyed, unusable, etc)
	// 1 - Normal
	// 2 - Magical
	// 3 - Epic
	// 4 - Artifact
	public function getQuality() { return $this->item['quality']; }

	// Item Composition: (Base Item Material)
	// 0 - None
	// 1 - Metal
	// 2 - Wood
	// 3 - Hemp
	// 4 - Leather
	// (Glass, Plastic, Cotton, Gold, Silver, Platinum, etc)
	public function getCompsition() { return $this->item['composition']; }

	// Item Material: (Exotic)
	public function getMaterial() { return $this->item['material']; }

	// Item Grades: (Homebrew Masterwork rules - neat)
	// 0 - Normal
	// 1 - Masterwork
	// 2 - 2nd Degree Masterwork
	// 3 - 3rd Degree Masterwork
	// 4 - 4th Degree Masterwork
	// 5 - 5th Degree Masterwork
	public function getGrade() { return $this->item['grade']; }

	// Item Slots:
	// 0 - Slotless
	// 1 - Head (Headband, Helmet, Phylactery, Hat)
	// 2 - Face (Lenses, Goggles, Mask)
	// 3 - Torso (Vest, Vestment, Shirt)
	// 4 - Back (Cloak, cape, mantle)
	// 5 - Throat (Amulet, brooch, medallion, necklace, periapt, scarab	)
	// 6 - Arm (Bracers, Bracelets)
	// 7 - Hand (Gloves, Gauntlets)
	// 8 - Ring (Rings)
	// 9 - Body (Armor, Robe)
	// 10 - Waist (Belt, Girdle)
	// 11 - Legs (Boots)
	// 12 - Main Hand (Weapons)
	// 13 - Off-Hand (Secondary Weapons, Shields, Relics)
	public function getSlot() { return $this->item['slot']; }

	// Item Flags:
	// 1 - Fragile -> 50% reduction in hardness
	// 2 - Cursed -> apply a random negative effect
	// 3 - Stackable (Multiple items in one group e.g. a bundle of arrows)
	// 4 - Improvised Weapon (Most items can be improvised weapons (Fx: 1d4-1d6 based on weight, )) Maybe default?
	// 5 - Has a Suffix
	// 6 - Has a Prefix
	public function getFlags() { return $this->item['flags']; }



	public function getWeight() { return $this->item['weight']; }
	public function getPrice() { return $this->item['price']; }
	public function getStack() { return $this->item['stack']; }
	
	public function getSuffix() { return $this->item['suffix']; }
	
	// Setters
	public function setLoot($array) { $this->item = $array; }
	public function setName($string) { $this->item['name'] = $string; }
	public function setQuality($value) { $this->item['quality'] = $value; }
	public function setGrade($value) { $this->item['grade'] = $value; }
	public function setMaterial($material) { $this->item['material'] = $material; }
	public function setWeight($value) { $this->item['weight'] = $value; }
	public function setStack($value) { $this->item['stack'] = $value; }
	public function setPrefix($prefix) { $this->item['prefix'] = $prefix; }
	public function setSuffix($suffix) {
		$this->addFlag(5);
		$this->item['suffix'] = $suffix;
	}
	
	public function isMagical() { return ($this->getQuality() >= 1 ? TRUE : FALSE); }
	public function hasFlag($flag) { $flags = explode(",", $this->getFlags()); return (in_array($flag, $flags) ? true : false); }
	public function addFlag($flag) {
		if ($this->hasFlag($flag)) return;
		$this->item['flags'] .= ',' . $flag;
		//array_push($this->item['flags'], $flag);
	}
	public function addPrice($gold, $silver = 0, $copper = 0) { $this->item['price'] += $gold; }
	
	
	private function randomizeQuality( bool $value = false ) {
		$i = LOOT_QUALITY_NORMAL;
		$basepct = 70;
		do {
			if (chance($basepct)) {
				if ($this->getQuality() == $i)
					$this->setQuality($this->getQuality() + 1);
			}
			$i++;
			$basepct -= 10;
		} while ($i < LOOT_QUALITY_ARTIFACT);
	}

	protected function checkQuality() {
		if ($this->getQuality() > LOOT_QUALITY_NORMAL && $this->getGrade() < LOOT_GRADE_MASTERWORK)
			$this->setGrade(LOOT_GRADE_MASTERWORK);
		
		if ($this->getGrade() < LOOT_GRADE_MASTERWORK) {
			// 15% chance the non-magical item is of masterwork grade.
			if (chance(15)) {
				$this->setGrade(LOOT_GRADE_MASTERWORK);
			}
		}		
	}
	protected function checkWeight() {
		if ($this->getStack() > 1)
			$this->setWeight($this->getStack() * $this->getWeight());
	}
	protected function checkPrice() {
		if ($this->getGrade() == LOOT_GRADE_MASTERWORK) {
			$this->addPrice(50000);
		}
		if ($this->getStack() > 1) {
			$this->addPrice( $this->getPrice() * $this->getStack() );
		}
	}

	private function randomizeQuantity() {
		if ($this->hasFlag(LOOT_FLAG_STACKABLE)) {
			$stack = $this->getStack();
			if ($stack == 1) {
				// Chance to have atleast 2 in a stack.
				if (chance(50)) {
					$stack += 1;
				}
			}
			else
				$stack = dice($stack);
			
			$this->setStack($stack);
		}
	}
	
	public function __close() {
		$this->checkName();
	}
	private function checkName() {
		if ($this->hasFlag(LOOT_FLAG_SUFFIX)) {
			$prename = $this->getName();
			$sname = $this->getSuffix()['name'];
			$this->setName($prename . " of " . $sname);
		}
	}
	private function hasFlavor() {
		$flavor = $this->item['flavor_text'];
		if (!is_null($flavor)) return TRUE;
		return FALSE;
	}
	private function checkMisc() {
		if ($this->hasFlavor()) {
			$this->item['flavor_text'] = '"' . $this->item['flavor_text'] . '"';
		}
	}

	// Item Generation Functions
	public function fetchTemplate(int $category) {
		$tables = array(
			2 => "item_template_weapon",
			5 => "item_template_container"
		);
		$table = $tables[$category];
		$stmt = $this->db->prepare(sprintf(
		"	SELECT *
			FROM item_template
			LEFT JOIN item_icons
			ON item_template.display_id = item_icons.display_id
			LEFT JOIN `%s`
			ON item_template.entry = `%s`.entry
			WHERE item_template.category = %d
			ORDER BY (RAND() - item_template.chance)
			LIMIT 1
		", $table, $table, $category));
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$result = randomElementTop($rows); 
		return $result;
	}

	// Materials (Adamantine, etc)
	public function fetchMaterial($composition) {
		if (empty($composition)) return;

		if ($this->getQuality() == LOOT_QUALITY_NORMAL)
			$stmt = $this->db->prepare(sprintf("SELECT * FROM item_materials WHERE `material_id` = %d", $composition));
		else
			$stmt = $this->db->prepare(sprintf("SELECT * FROM item_materials WHERE `filter` = %d", $composition));

		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$material = randomElement($rows);
		return $material;
	}
}
?>