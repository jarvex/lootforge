<?php
class Armament extends Item
{
	public function __close() { parent::__close(); }
	public function __construct($category) {
		parent::__construct($category);

		$this->item['enhancement'] = array();
		$this->item['enhancement']['bonus'] = 0;
		$this->item['enhancement']['special'] = array();
	}
	public function getDurability() { return $this->item['durability']; }
	public function setDurability($value) { $this->item['durability'] = $value; }
	
	public function getSpecial() {}
}
?>