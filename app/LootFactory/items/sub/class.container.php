<?php
class Container extends Item
{
	private $max_depth = 2;		// Maximum depth allowed for the initial starting container. (What we want to achieve)
	private $cur_depth = 1;		// Current depth of container. Containers inside containers.
	private $max_load = 10;		// Maximum amount of loot that can be stored inside one container.
	private $total_value = 0; 	// Net worth of all combined items in the container.
	
	// Constructor
	public function __construct($depth) {
		parent::__construct(LOOT_CATEGORY_CONTAINER);
		$this->setDepth($depth);
		$this->item['contents'] = array();
		
		$this->fill();
		$this->item['total_value'] = $this->total_value;
		
		parent::__close();
	}
	
	// Getters
	public function getDepth() { return $this->cur_depth; }
	public function getMaxDepth() { return $this->max_depth; }
	public function getMaxLoad() { return $this->max_load; }
	public function getContents() { return $this->contents; }
	public function holdsLiquid() { return $this->item['holds_liquid']; }	
	// Setters
	public function setDepth($value) { $this->cur_depth = $value; }
	
	// Adders
	public function addItem($item = array()) {
		$loot = array("loot" => $item);
		array_push($this->item['contents'], $loot);
		$this->total_value += $item['price'];
	}
	
	// Main Functions
	public function addLiquid() {
		
	}
	
	public function fill() {
		// Amount of items that can be found inside the container.
		$count = mt_rand(0, $this->getMaxLoad());
		if (empty($count)) {
			$this->setName("Empty " . $this->getName());
			return;
		}

		if ($this->holdsLiquid()) {
			$percent = $count * 10;
			if ($this->isMagical()) {
				
			}
			else {
				
			}
			$array = array(
				array('name' => 'Water'),
				array('name' => 'Pepsi')
			);
			$this->setSuffix(randomElement($array));
			
		}
		else {
			for($i = 0; $i < $count; $i++) {
				// Fetch an item template from our loot factory.
				$category = LootFactory::getCategory();
				// Figure out if it's a container first?
				if ($category == LOOT_CATEGORY_CONTAINER) {
					if ($this->getDepth() >= $this->getMaxDepth()) {
						do {
							$category = LootFactory::getCategory();
						} while ($category == LOOT_CATEGORY_CONTAINER);
						$item = LootFactory::getContainer();
					} else
						$item = LootFactory::getContainer($this->getDepth() + 1);
				} else
					$item = LootFactory::getTemplate($category);
				// Add the item template to our container.
				$this->addItem($item->getLoot());
			}
		}
	}
}
?>