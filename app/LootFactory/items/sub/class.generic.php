<?php
class Generic extends Item
{
	public function __construct() {
		parent::__construct();
		$loot = Tables::instance()->getLootTable();
		$result = randomElement($loot);
		$this->setLootObject($result);
		
		parent::__close();
	}
}
?>