<?php
class Weapon extends Armament
{
	private $weapon = array();

	// Offensive Weapon Attributes
	// effects : array
	//	event : onAttack, onHit, Activation, onCast, etc
	//	effect : string
	// damage : array
	// threat_range : value 
	public $offense = array(
		'effects' => array(),
		'damage' => array(),
		'attack' => array(),
		'threat_range' => 0
	);
	
	public function __construct($category) {
		parent::__construct($category);
		
		if ($this->getQuality() > LOOT_QUALITY_NORMAL)
			$this->upgrade();
		
		parent::__close();
	}


	public function addDamage($value) { array_push($this->offense['damage'], $value); }
	public function addAttack($value) { array_push($this->offense['attack'], $value); }

	public function setEnhancementBonus($value) { $this->item['enhancement']['bonus'] = $value; }
	public function addSpecial($addon) { array_push($this->item['enhancement']['special'], $addon); }

	private function upgrade() {
		
		
		$this->addPrefix();
		$this->addSuffix();
		$this->addEnhancements();


		// if (!is_null($this->item['parts'])) {
		// 	$parts = explode(",", $this->item['parts']);
		// 	$tmp = array();
		// 	foreach ($parts as $key => $value) {
		// 		$material = (dice(100) > 50 ? $this->addMaterial() : false);
		// 		// $grade = (dice(100) > 50 ? $this->addGrade() : false);
		// 		$part = array(
		// 			'name' => $value,
		// 			// 'grade' => $grade,
		// 			'material' => $material,
		// 		);
		// 		array_push($tmp, $part);
		// 	}
		// 	$this->item['parts'] = $tmp;
		// }
		// else
		
		$this->setGrade(dice(5));

		// if (chance(50))
		//$this->setMaterial($this->fetchMaterial($this->getComposition()));
		
		//$this->updateGrade();


		//$this->updateDPR();
	}

	private function updateGrade() {
		if ($this->getGrade() > LOOT_GRADE_NORMAL) {
			switch($this->getGrade()):
				case LOOT_GRADE_MASTERWORK:
					$gold = 300;
					break;
				case 2:
					$gold = 9000;
					break;
				case 3:
					$gold = 27000;
					break;
				case 4:
					$gold = 810000;
				case 5:
					$gold = 24300000;
					break;
			endswitch;
			$this->addPrice($gold, 0, 0);
		}
	}
	
	private function addPrefix() {
		$stmt = $this->db->prepare(sprintf("SELECT * FROM item_addon_affix WHERE `type` = %d AND `item_cat` = %d", LOOT_PREFIX, LOOT_CATEGORY_ARMAMENT));
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$prefix = randomElement($rows);
		
		$this->setPrefix($prefix);
		$this->addPrice($prefix['value']);
	}
	
	private function addSuffix() {
		$stmt = $this->db->prepare(sprintf("SELECT * FROM item_addon_affix WHERE `type` = %d AND `item_cat` = %d", LOOT_SUFFIX, LOOT_CATEGORY_ARMAMENT));
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$suffix = randomElement($rows);
		
		$this->setSuffix($suffix);
		$this->addPrice($suffix['value']);
	}
	
	private function updateDPR() {
		if ($this->getGrade() > LOOT_GRADE_NORMAL) {
			$this->addDamage($this->getGrade());
		}

		

		$this->item['offense'] = $this->offense;
	}

	// Weapons cannot exceed an enhancement bonus of +5
	// HOWEVER, special abilities can be added in to a maximum of +10
	public function addEnhancements() {
		$slots = 0;
		switch($this->getQuality()) {
			case LOOT_QUALITY_ARTIFACT:
				$slots = diceLowBall(15, 25);
			break;
			case LOOT_QUALITY_EPIC:
				$slots = diceLowBall(LOOT_ENHANCEMENT_BONUS_MAXIMUM, 15);
			break;
			default:
				$slots = diceLowBall(1, LOOT_ENHANCEMENT_BONUS_MAXIMUM);
			break;
		}

		$bonus = 0;
		$special = 0;

		do {
			$dice = dice(100);
			if ($dice >= 50)
				$special += 1;
			else
				$bonus += 1;
			$slots--;
		}
		while($slots > 0);

		if ($this->getQuality() < LOOT_QUALITY_ARTIFACT) {
			if ($bonus > 5) {
				$tmp = $bonus;
				$special += ($bonus + 1) - $tmp;
				$bonus = 5;
			}
		}

		if ($special > 0 && $bonus == 0) {
			$special -= 1; 
			$bonus += 1;
		}

			if ($special > 0) {
				$mods = array(0);
				do {
					$stmt = $this->db->prepare(sprintf("SELECT * FROM `item_addon_special`
							WHERE `special_bonus` <= {$special}
							AND (`special_id` NOT IN (%s))
							ORDER BY RAND()
							LIMIT 1", implode(",", $mods)));

					$stmt->execute();
					$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
					$addon = randomElement($rows);

					if (empty($addon)) {
						$bonus += $special;
						break;
					}


					//if ($addon['damage'])
					//	$this->updateSchool($addon['damage'], $addon['damage_school']);

					$special -= $addon['special_bonus'];
					array_push($mods, $addon['special_id']);

					//if ($addon['id'] == 6)
						//$this->isKeen = TRUE;

					$this->addSpecial($addon);
				}
				while($special > 0);
			}

			if ($bonus > 0)
				$this->setEnhancementBonus($bonus);

			// $dice = dice(100);
			// switch($dice):
			// 	case ($dice >= 1 && $dice <= 33): // Minor
			// 		$dice = dice(100);
			// 		switch($dice):
			// 			case ($dice >= 1 && $dice <= 70):
			// 		endswitch;
			// 		break;
			// 	case ($dice >= 34 && $dice <= 67): // Medium
			// 		break;
			// 	case ($dice >= 68 && $dice <= 100): // Major
			// 		break;
			// endswitch;



		// else // Open-ended Enhancements!
		// {
		// 	$sql = "SELECT * FROM `loot_addon_weapon_enhancement` WHERE (`damage_type_mask` & :bit) > 0";
		// 	$enhancements = $this->db->query($sql)
		// 					->bind(":bit", $this->getDamageType())
		// 					->resultSet();

		// 	$times = mt_rand(1, 10);
		// 	$slots = 0;
		// 	for($i = 1; $i <= $times; $i++)
		// 	{
		// 		$dice = dice(50);
		// 		if ($dice == 50)
		// 			$slots += 10;

		// 		$slots++;
		// 		if ($slots >= 24)
		// 			break;
		// 	}
		// 	$tmp = array();
		// 	$slottmp = 0;
		// 	for($i = 1; $i <= $slots; $i++)
		// 	{
		// 		shuffle($enhancements);
		// 		$result = array_shift($enhancements);

		// 		$slottmp += $result['space'];
		// 		$tmp[] = $result;
		// 		if ($slottmp >= $slots)
		// 			break;
		// 	}
		// 	$weapon['enhancementMods'] = $tmp;
		// 	$weapon['enhancement_bonus'] = $slots;
		// 	foreach($weapon['enhancementMods'] as $e)
		// 		if (is_null($e['damage']) == FALSE)
		// 			$weapon = $this->updateSchool($weapon, $e['damage'], $e['damageSchool']);

		// 	return $weapon;
		// }
	}

}
?>