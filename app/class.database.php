<?php
class Database {
	
	protected static $instance;
 
	public function __construct() {}
	
	public static function getInstance() {
	
		if(empty(self::$instance)) {
			
			$db_info = array(
				"db_host" => "localhost",
				"db_port" => "3306",
				"db_user" => "root",
				"db_pass" => "yoda24",
				"db_name" => "dnd"
			);
			
			self::$instance = new PDO("mysql:host=".$db_info['db_host'].';port='.$db_info['db_port'].';dbname='.$db_info['db_name'], $db_info['db_user'], $db_info['db_pass']);
			
		}
		
		return self::$instance;
	
	}
 
}
?>