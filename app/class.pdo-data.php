<?php
class Tables
{
	/**
	* 
	* @var Singleton
	*/
	private static $instance;
	private static $table_item_template;
	private static $table_item_template_weapon;
	private static $table_item_icons;

	private function __construct() {
		$db = Database::getInstance();
		$stmt = $db->prepare("SELECT * FROM item_template");
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		self::$table_item_template = $rows;
		
		$stmt = $db->prepare("SELECT * FROM item_template_weapon");
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		self::$table_item_template_weapon = $rows;
		
		$stmt = $db->prepare("SELECT * FROM item_icons");
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		self::$table_item_icons = $rows;
	}

	public static function getInstance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function getItemTemplate() { return self::$table_item_template; }
	public function getItemTemplateWeapon() { return self::$table_item_template_weapon; }
	public function getItemIcons() { return self::$table_item_icons; }

	public function someMethod2() {
	// whatever
	}
}
?>