<?php
class Character
{
	public $character = array();
	public $json_character = '';
	public $inventory;
	
	public function __construct() {
		$this->inventory 		= new Inventory();
		$this->database 		= Registry::getInstance()->set('database', Database::getInstance());

	}
	public function create() {
		$count = dice(5) + 25;
		for($i = 0; $i < $count; $i++) {
			$item = LootFactory::getTemplate(LOOT_CATEGORY_RANDOM);
			$this->inventory->add($item->getLoot());
		}
		$this->save();
		
		return $this->getCharacter(false);
	}
	
	private function save() {
		$this->character['inventory'] = $this->inventory->retrieve();
		
		$this->json_character = json_encode($this->character, TRUE);
	}
	
	private function getCharacter($bool = false) { return ($bool ? $this->json_character : $this->character); }
}
?>