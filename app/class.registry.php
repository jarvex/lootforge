<?php
class Registry {    

        private static $_instance;
        private $_map;
        

        private function __construct()
        {}
        
        public static function getInstance()
        {
                if(self::$_instance === null)
                {
                        //First and only construction.
                        self::$_instance = new self();
                }
                return self::$_instance;
        }
        
        public function get($key)
        {
                return $this->_map[$key];
        }
        
        public function set($key, $object)
        {
                return $this->_map[$key] = $object;
        }
        
        private function __clone()
        {}
}
?>