<?php
define("LOOT_ENHANCEMENT_BONUS_MAXIMUM", 5);

define("LOOT_QUALITY_NORMAL", 1);
define("LOOT_QUALITY_MAGICAL", 2);
define("LOOT_QUALITY_EPIC", 3);
define("LOOT_QUALITY_ARTIFACT", 4);

define("LOOT_GRADE_NORMAL", 0);
define("LOOT_GRADE_MASTERWORK", 1);

define("LOOT_STYLE_RANDOM", 0);
define("LOOT_STYLE_FIXED", 1);

define("LOOT_PREFIX", 0);
define("LOOT_SUFFIX", 1);

define("LOOT_CATEGORY_RANDOM", 0);
define("LOOT_CATEGORY_GENERIC", 1); // General store items, currency, trade goods, etc
define("LOOT_CATEGORY_ARMAMENT", 2); // Weapons, Armor (heavy, light, shield)
define("LOOT_CATEGORY_WEAPON", 2);
define("LOOT_CATEGORY_POTION", 3);
define("LOOT_CATEGORY_", 2);
define("LOOT_CATEGORY_CONTAINER", 5);
define("LOOT_CATEGORY_CREATURE", 4);

define("LOOT_GENERIC_SUBCATEGORY_",1);

define("LOOT_FLAG_CURSED", 1);
define("LOOT_FLAG_INTELLIGENT", 2);
define("LOOT_FLAG_STACKABLE", 3);
define("LOOT_FLAG_SUFFIX", 5);
define("LOOT_FLAG_PREFIX", 6);


// LOOT SPECIFICS: WEAPONS
define("LOOT_WEAPON_SPECIAL_FRAGILE", 1);
define("LOOT_WEAPON_SPECIAL_DISARM", 2);
define("LOOT_WEAPON_SPECIAL_REACH", 3);
define("LOOT_WEAPON_SPECIAL_TRIP", 4);
define("LOOT_WEAPON_SPECIAL_GRAPPLE", 5);
define("LOOT_WEAPON", 5);
define("LOOT_WEAPON_SPECIAL_PERFORMANCE", 5);
define("LOOT_WEAPON_SPECIAL_NONLETHAL", 6);
define("LOOT_WEAPON_SPECIAL_IMPROVISED", 7);
define("LOOT_WEAPON_SPECIAL_", 5);
?>