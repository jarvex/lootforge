<?php
function dice($ceiling) { $result = mt_rand(1, $ceiling); return $result; }
function diceLowBall($min = 1, $max) {
    $roll1 = mt_rand($min, $max);
    $roll2 = mt_rand($min, $max);
    $end1 = min($roll1, $roll2);
    $roll3 = mt_rand($min, $max);
    $roll4 = mt_rand($min, $max);
    $end2 = min($roll3, $roll4);
    $end = min($end1, $end2);
    return $end;
}
function chance($pct) {	
	$curPct = floor(($pct * 100) + .5); 
	$MAX = 10000; 
	$pctArray = array();
	$num = 0;
	$flag = 0;
	if ($curPct <= 0) { $num = 0; }
	else if ($curPct >= $MAX) { $num = 1; }
	else {
		for ($i = 1; $i <= $MAX; $i++) {
			if ($i <= $curPct) { $pctArray[] = 1; }
			else { $pctArray[] = 0; }
		}
		shuffle($pctArray);
		$rand = mt_rand(0,9999); 
		$num = $pctArray[$rand];
	}
	if ($num == 1) { $flag = 1; return $flag; }
	else { $flag = 0; return $flag; }
}
function randomElement($array) { return $array[array_rand($array)]; }
function randomElementWeighted(array $weightedValues) {
	$rand = mt_rand(1, (int) array_sum($weightedValues));
	foreach ($weightedValues as $key => $value) {
		$rand -= $value;
		if ($rand <= 0) {
			return $key;
		}
	}
}
function array_join($original, $merge, $on) {
    if (!is_array($on)) $on = array($on);
    foreach ($merge as $remove => $right) {
        foreach ($original as $index => $left) {
            foreach ($on as $from_key => $to_key) {
                if (!isset($original[$index][$from_key])
                || !isset($right[$to_key])
                || $original[$index][$from_key] != $right[$to_key])
                    continue 2;
            }
            $original[$index] = array_merge($left, $right);
            unset($merge[$remove]);
        }
    }
    return array_merge($original, $merge);
}
function preprint($s, $return=false) { 
	$x = "<pre>"; 
	$x .= print_r($s, 1); 
	$x .= "</pre>"; 
	if ($return) return $x; 
	else print $x; 
}
function hasPreVowel($string) { (preg_match('/^[aeiou]/i', $string) ? TRUE : FALSE); }
?>