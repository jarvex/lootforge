<?php
include 'class.database.php';
include 'class.pdo-data.php';
include 'functions.php';
include 'constants.php';
include 'class.person.php';
include 'class.inventory.php';
include 'class.registry.php';

include 'LootFactory/factory.loot.php';
include 'LootFactory/items/class.item.php';
include 'LootFactory/items/sub/class.armament.php';
include 'LootFactory/items/sub/class.weapon.php';
include 'LootFactory/items/sub/class.generic.php';
include 'LootFactory/items/sub/class.container.php';
?>