<?php
	include 'app/include.php';
	$person=new Character();
	$background = $person->create();

	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;
?>
<html>
	<head>
		<script src="/static/js/jquery/jquery-2.2.1.min.js"></script>
		<script src="/static/js/jquery/jquery.qtip.min.js"></script>
		<script src="/static/js/bootstrap/bootstrap.min.js"></script>
		<script src="/static/js/underscore-min.js"></script>
		<script src="/static/js/underscore-helpers.js"></script>
		<link href="/static/css/vendor/reset.css" rel="stylesheet" type="text/css">
		<link href="/static/css/vendor/normalize.css" rel="stylesheet" type="text/css">
		<link href="/static/css/main.css" rel="stylesheet" type="text/css">
		<link href="/static/css/character/char.css" rel="stylesheet" type="text/css">
		<link href="/static/css/tooltip.css" rel="stylesheet" type="text/css">
		<link href="/static/css/global.css" rel="stylesheet" type="text/css">
		<title></title>
		<script>
$(document).ready(function () {
	var templates = {
		'core': {
			'ttip': {
				2: (
					'<div class="tooltip-weapon">' +
						'<span class="icon-frame frame-56" style="background-image: url(/static/images/character/inventory/items/<%= loot.icon %>.png);"></span>' +
						'<h3 class="loot-q<%= loot.quality %>"><% (loot.stack > 1 ? print(loot.stack+" "+loot.name+"s") : print(loot.name)) %></h3>' +
						'<ul class="tooltip-list">' +
							'<li><%= fnParseEnhancements(loot.enhancement) %></li>' +
							'<li><%= fnParseGrade(loot.grade) %> <%= (loot.material !== null ? loot.material.name : fnParseComposition(loot.composition)) %><span class="fl-right"> <%= fnParseQuality(loot.quality) %></span></li>' +
							'<li><%= fnParseGrouping(loot.grouping) %>, <%= fnParseHandle(loot.handle) %> <%= fnParseCategory(loot.category) %><span class="fl-right"><%= fnParseWeaponClass(loot.wpn_class) %></span></li>' +
							'<li>1-2 <%= fnParseDamageType(loot.damage_type) %> DPR <span class="fl-right"><%= fnParseThreat(loot.threat_range) %>, x<%= loot.crit_mult %></span></li>' +
							'<% if (loot.special) { %>' +
							'<li class="special"><%= fnParseWeaponSpecial(loot.special, true) %></li>' +
							'<% } %>' +
							'<% if (loot.flavor_text) { %>' +
							'<li class="flavor-text"><%= loot.flavor_text %></li>' +
							'<% } %>' +
							'<br>' +
							'<li>Weight: <span class="fl-right"><%= loot.weight %> lbs</span>' +
							'<li>Market Price: <span class="fl-right"><%= fnParsePrice(loot.price) %></span></li>' +
							'<% if (loot.description) { %>' +
							'<li class="description"><%= loot.description %></li>' +
							'<% } %>' +
							'<span class="clear"></span>' +
						'</ul>' +
					'</div>'
				),
				5: (
					'<div class="tooltip-container">' +
						'<span class="icon-frame frame-56" style="background-image: url(/static/images/character/inventory/items/<%= loot.icon %>.png);"></span>' +
						'<h3 class="loot-q<%= loot.quality %>"><%= loot.name %></h3>' +
						'<ul class="tooltip-list">' +
							'<li><%= fnParseCategory(loot.category) %></li>' +
							'<li><%= fnParseContents(loot.contents) %></li>' +
							'<li><%= fnParsePrice(loot.price) %></li>' +
						'</li>' +
					'</div>'
				),			
			},
			'wiki': {
				2: (
					'<div class="wiki">' +
						'<div class="info">' +
							'<div class="title">' +
								'<h2 class="loot-q<%= loot.quality %>"><%= loot.name %></h2>' +
							'</div>' +
							'<div class="item-detail">' +
								'<span class="icon-frame frame-56" style="background-image: url(/static/images/character/inventory/items/<%= loot.icon %>.png);"></span>' +
								'<ul class="item-specs">' +
									'<li><%= fnParseGrade(loot.grade) %> <%= (typeof(loot.material) !== "undefined" ? loot.material.name : "") %> <%= fnParseCategory(loot.category) %>' +
									'<li><%= fnParseGrouping(loot.grouping) %>, <%= fnParseHandle(loot.handle) %><span class="fl-right"><%= fnParseWeaponClass(loot.wpn_class) %></span></li>' +
									'<li>1 - 2 DPR <%= fnParseDamageType(loot.damage_type) %><span class="fl-right"><%= fnParseThreat(loot.threat_range) %>, x<%= loot.crit_mult %></span></li>' +
									'<li>Weight: <span class="fl-right"><%= loot.weight %></span>' +
									'<li>Base Price: <span class="fl-right"><%= fnParsePrice(loot.price) %></span></li>' +
									'<% if (loot.description) { %>' +
									'<li class="description"><%= loot.description %></li>' +
									'<% } %>' +
									'<span class="clear"></span>' +
								'</ul>' +
							'</div>' +
						'</div>' +
					'</div>'
				),
				5: (
					'<div class="wiki">' +
						'<div class="info">' +
							'<div class="title">' +
								'<h2 class="loot-q<%= loot.quality %>"><%= loot.name %></h2>' +
							'</div>' +
							'<div class="item-detail">' +
								'<span class="icon-frame frame-56" style="background-image: url(/static/images/character/inventory/items/<%= loot.icon %>.png);"></span>' +
								'<ul class="item-specs">' +
									'<li><%= fnParseCategory(loot.category) %></li>' +
									'<li><%= fnParseContents(loot.contents) %></li>' +
									'<li><%= fnParsePrice(loot.price) %></li>' +
								'</ul>' +
							'</div>' +
						'</div>' +
					'</div>'
				),
			},
			'skill': {},
		},
		'partials': {
			
		}
	};
	
    $(document).on('mouseover', '.loot-ttip, .wiki-container', function() {
		var $t = $(this);
		
		var item = JSON.parse($(this).attr('data-tooltip'));
		var template = _.template(templates.core.ttip[$(this).attr('data-template')]);
		var content = template(item);
        $(this).qtip({
			overwrite: false,
			prerender: true,
            content: {
                text: content
            },
			position: {
				my: 'bottom left',
				at: 'top right',
				viewport: $('body'),
				adjust: {
					method: 'flip',
					screen: true,
					x: 25,
					y: -15
				},
				effect: false,
			},
			show: { delay: 80, event: 'mouseenter', solo: true, ready: true },
			hide: { delay: 20, event: 'mouseleave click', fixed: true },
			style: { classes: 'dnd-tooltip', tip: false }
        });
    }).on('click', '.loot-ttip, .wiki-container', function() {
		var item = JSON.parse($(this).attr('data-tooltip'));
		var template = _.template(templates.core.wiki[$(this).attr('data-template')]);
		var content = template(item);
		  var $this = $(this),
			  $bc = $('<li></li>');

		  $this.parents('li').each(function(n, li) {
			  var $a = $(li).children('a').clone();
			  $bc.prepend(' / ', $a);
		  });
			$('#breadcrumb').html( $bc.prepend('<li class="first"><a href="#"><span></span>Main</a></li>') );	
		
		$('#inv-contents').hide();
		$('#inv-wiki').html(content).fadeIn();
	});
});
		</script>
	</head>
	<br><br><br><br><br><br><br><br><br>
	<body>
		<div class="page-body" style="width: 60%; margin: 0px auto">
			Loot Box 
			<div class="inventory-table" id="personal-inventory">
				<div class="inventory-row">
					<div class="inventory-cell">
						<div class="inventory-item fire-bow1" data-item-type="weapon bow enchanted"></div>
					</div>
					<div class="inventory-cell"></div>
				</div>
				<div class="inventory-row">
					<div class="inventory-cell"></div>
					<div class="inventory-cell"></div>
				</div>
			</div>
			<ul class="inv-container" id="personal-inventory">
				<div id="inv-wiki"></div>
				<div id="inv-contents">
				<?php
					foreach($background['inventory'] as $idx) {
					
					?>
					<li class="inv-gridbox"><div style="background-image: url('/static/images/character/inventory/items/<?php echo $idx['loot']['icon']; ?>.png');" class="inv-item loot-ttip border-q<?php echo $idx['loot']['quality']; ?>" data-template="<?php echo $idx['loot']['category']; ?>" data-tooltip="<?php echo htmlspecialchars(json_encode($idx, TRUE)); ?>"><?php if ($idx['loot']['stack'] > 1) { echo '<span class="stack">'.$idx['loot']['stack'].'</span>'; } ?></div></li>
					<?php
					}
				?><span class="clear"><!-- --></span>
				</div>
			</ul>
			<?php
				$time = microtime();
				$time = explode(' ', $time);
				$time = $time[1] + $time[0];
				$finish = $time;
				$total_time = round(($finish - $start), 4);
				echo 'Loot generated in '.$total_time.' seconds.';
			?>
		</div>
	</body>
</html>