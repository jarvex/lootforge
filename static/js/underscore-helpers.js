_.addTemplateHelpers( {
    fnParseCategory: function( category ) {
		var categories = {
			1: "General",
			2: "Weapon",
			5: "Container"
		};
		return categories[category];
    },
	escapeHtml: function(text) {
	  var map = {
	    '&': '&amp;',
	    '<': '&lt;',
	    '>': '&gt;',
	    '"': '&quot;',
	    "'": '&#039;'
	  };

	  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
	},
	fnParseQuality: function ( quality ) {
		var qualities = {
			0: "",
			1: "",
			2: "Magical",
			3: "Epic",
			4: "Artifact"
		};
		return qualities[quality];
	},
	fnParseContents: function ( contents ) {
		var count = contents.length;
		var html = "";
		html += '<ul class="container-contents">';
		for (var i = 0; i < count; i++) {
			if (contents[i].loot.category == 5) {
				html += '<li class="wiki-container" data-template="'+contents[i].loot.category+'" data-tooltip="'+this.escapeHtml(JSON.stringify(contents[i]))+'">' +
					'<span class="icon-frame list frame-16" style="background-image: url(/static/images/character/inventory/items/'+contents[i].loot.icon+'.png);"></span>' +
					'<span class="loot-q'+contents[i].loot.quality+'">'+(contents[i].loot.stack > 1 ? contents[i].loot.stack+" "+contents[i].loot.name+"s" : contents[i].loot.name); '</span>' +
					'</li>';
			} else {
				html += '<li class="loot-ttip" data-template="'+contents[i].loot.category+'" data-tooltip="'+this.escapeHtml(JSON.stringify(contents[i]))+'">' +
					'<span class="icon-frame list frame-16" style="background-image: url(/static/images/character/inventory/items/'+contents[i].loot.icon+'.png);"></span>' +
					'<span class="loot-q'+contents[i].loot.quality+'">'+(contents[i].loot.stack > 1 ? contents[i].loot.stack+" "+contents[i].loot.name+"s" : contents[i].loot.name); '</span>' +
					'</li>';
			}
		}
		html += '</ul>';
		return html;
	},
	fnParseWeaponClass: function( value ) {
		var classes = {
			0: "Other",
			1: "Axe",
			2: "Bow",
			3: "Crossbow",
			4: "Dagger",
			5: "Flail",
			6: "Hammer",
			7: "Mace",
			8: "Pick",
			9: "Sling",
			10: "Spear",
			11: "Staff",
			12: "Sword",
			13: "Unarmed",
			14: "Whip"
		};
		return classes[value];
	},
	fnParseGrouping: function( group ) {
		var groups = {
			1: "Simple",
			2: "Martial",
			3: "Exotic"
		};
		return groups[group];
	},
	fnParseHandle: function( handle ) {
		var handles = {
			0: "Unassigned",
			1: "Light",
			2: "One-Handed",
			3: "Two-Handed",
			4: "Ranged",
			5: "Ammunition"
		};
		return handles[handle];
	},
	fnParsePrice: function( money ) {
		
		var copper = money % 10;
		money = (money - copper) / 10;
		var silver = money % 10;
		money = (money - silver) / 10;
		var gold = money % 10;
		money = (money - gold) / 10;
		var platinum = money;

		var string = '<ul class="currency">';
		if (platinum > 0) { string += '<li class="platinum">'+platinum+'</li>'; }
		if (gold > 0) { string += '<li class="gold">'+gold+'</li>'; }
		if (silver > 0) { string += '<li class="silver">'+silver+'</li>'; }
		if (copper > 0) { string += '<li class="copper">'+copper+'</li>'; }
		string += '</ul>';
		return string;
	},
	fnParseThreat: function ( value ) {
		if (value < 20)
			return value + "-20";
		else
			return value;
	},
	fnParseDamageType: function ( value ) {
		var types = {
			1: "Slashing",
			2: "Bludgeoning",
			4: "Piercing"
		};
		var string = "";
		for(var index in types) {
			if (value & index) {
				string += types[index];
			}
		}
		return string;
	},
	fnParseWeaponSpecial: function(flag, verbose) {
		var flags = {
			1: "Disarm",
			2: "Trip",
			4: "Nonlethal",
			8: "Brace",
			16: "Reach",
			32: "Monk",
			64: "Double",
			128: "Blocking",
			256: "Grapple",
			512: "Distracting",
			1024: "Performance",
			2048: "Sunder"
		};
		if (verbose == true) {
			var string = "";
			for(var index in flags) {
				if (flag & index) {
					string += flags[index];
				}
			}
			return string;
		} else {
			var count = 0;
			for(var index in flags) {
				if (flag & index) {
					count++;
				}
			}
			return count;
		}
	},
	fnParseGrade: function( value ) {
		var string = "";
		if (value > 0) {
			string = "Masterwork";
			if (value > 1) {
				string += "<sup>"+value+"</sup>";
			}
		}
		return string;
	},
	fnParseComposition: function( value ) {
		var compositions = {
			0: "",
			1: "Metal",
			2: "Wood",
			3: "Hemp",
			4: "Leather",
			5: "Glass"
		};
		return compositions[value];
	},
	fnParseParts: function( parts ) {
		var count = parts.length;
		var html = "";
		html += '<ul>';
		for (var i = 0; i < count; i++) {
			html += '<li>'
			+ '<span>'+parts[i].name+'</span>'
			+ '</li>';
		}
		html += '</ul>';
		return html;
	},
	fnParseEnhancements: function( enhancement ) {
		var string = "";
		if (enhancement.bonus > 0) {
			string += "+" + enhancement.bonus;
		}
		if (enhancement.special) {
			string += " ";
			var names = [];
			for (var key in enhancement.special) {
				var obj = enhancement.special[key];
				names.push(obj.name);
			}
			names.join(", ");
			string += names;
		}
		return string;
	}
});